import cv2

capture = cv2.VideoCapture(0)
config_file = 'ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'
frozen_model = 'frozen_inference_graph.pb'
model = cv2.dnn_DetectionModel(frozen_model, config_file)
classLabels = []
filename = 'Labels.txt'
with open(filename, 'rt') as f:
    classLabels = f.read().rstrip('\n').split('\n')

model.setInputSize(320, 320)
model.setInputScale(1.0 / 127.5)
model.setInputMean((127.5, 127.5, 127.5))
model.setInputSwapRB(True)

while True:
    ret, frame = capture.read()
    classIndex, confidece, bbox = model.detect(frame, confThreshold=0.5)
    if len(classIndex) != 0:
        if 16 in classIndex:
            print(classLabels[15])

    cv2.imshow('human', frame)
    if cv2.waitKey(20) & 0xFF == ord('d'):
        break
capture.release()
cv2.destroyAllWindows()
